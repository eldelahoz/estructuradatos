package Burbuja;

import java.util.Iterator;

public class OrdenamientoB {

	private long[] arr; // Array
	private int numE; // Numero de elementos.

	public OrdenamientoB(int size) {
		arr = new long [size];
		numE = 0;
	}
	
	public void setValue(long value) {
		arr[numE] = value;
		numE ++;
	}
	
	public void getValue() {
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}
	
	public void ordenBurbuja() {
		for (int i = numE-1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if(arr[j] > arr[j+1]){
					long temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
	}
	
	public void ordenSelect() {
		int x; //Variable x para guardar la posicion
		long m; //Variable m para guardar el dato menor
		for (int i = 0; i < numE; i++) {
			m = arr[i];
			x=i;
			for (int j = i+1; j < numE; j++) {
				if(arr[j] < m) { //Se compara si hay un dato menor que m
					m = arr[j]; // Se guarda el dato menor
					x = j; // Se guarda la posicion del dato
				}
			}
			//Se hacen los intercambios
			arr[x] = arr[i];
			arr[i] = m; //Se manda a adelante.
		}
	}
}
